## installation

#### Ubuntu

```
sudo apt install cura
```
(way faster startup time than app image)

#### app image

There is an app image on github -> releases:
```
https://github.com/Ultimaker/Cura/releases
```

To run app images you need to install `libfuse2`:
```
sudo apt install libfuse2
```

Download the latest app image.
```
chmod +x UltiMaker-Cura-5.7.1-linux-X64.AppImage
```

```
./Ultimaker-Cura-5.7.1-linux-X64.AppImage
```

The Appimage has the advantage that it does not have the annoying \
errors at startup about plugins not loading at startup.\
But the startup time is about 3 times higher than when installed via package manager...

#### start gcode and end gcode

preferences -> configure cura -> printers -> select your printer \
-> machine settings

There you can see the "start gcode" that is executed before every print.\
And the "end gcode" that is executed after every print.

I edited the end gcode to move the Z axis further up after the print.
Let's see if it worked.

#### simulatneous heating of bed and extruder (use with caution)

add this to your start gcode:\
https://3dprinting.stackexchange.com/questions/12188/simultaneous-heating-of-extruder-and-bed-at-start-of-print

It worked, but you probably should not do this because your heatbed \
needs some time so heat spreads more evenly.
