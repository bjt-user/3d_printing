#### tutorials

https://wiki.freecad.org/Basic_Attachment_Tutorial

made it until:\
https://wiki.freecad.org/Basic_Attachment_Tutorial#A_Step_Further

It was very educational.\
It also teaches a little bit on why it can make sense to name your constraints \
and use expressions on these names for creating constraints for different sketches.\
Naming constraints seems to be especially useful when doing this in your base sketch.

#### array in sketch

Select one or more objects (for example a circle).\
Click "rectangular array".\
Set the number of rows and columns.\
(i.e. 4 rows and 4 columns for a grid)\
Click "ok".\
Now you have to select a spot where to put the second element.\
(This will determine the angle, but you can change the angle afterwards.)
Click somewhere on the sketch.

You will now see all the other elements.\
There are construction lines connecting the elements in horizontal direction.

Now search for the angle and double click the angle constraint.\
Set the angle constraint to 0!

#### sketch: center rectangle

Hit <kbd>s<kbd>.\
Select the one upper corner of the rectangle and then one bottom corner from the opposite side.\
So that if you would connect these two points the resulting line would go through the center of the sketch or plane.\
Now click on the center point or origin or however you want to call it.

This is faster than making two symmetries of each side.
