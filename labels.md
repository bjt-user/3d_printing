Pocketed a ShapeString with FreeCAD.

Horizontal print with 0.1 layer height.

The plan was to fill the pocket with paint.

A 26mm*10mm label (size 2mm ShapeString in Freecad) is not readable.

At 40mm*20mm (rough surface size of the text) 3d printed labels become readable.

3mm size shapestrings in Freecad are readable. (at least when printed vertically)

#### TODO: paint pocketed strings

Maybe use primer and then drip paint on the pocketed string with a pipette?