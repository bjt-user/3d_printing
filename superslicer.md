`superslicer` is part of the `extra` repository.
```
sudo pacman -S superslicer
```

For superslicer to work you need to configure the locale first.

```
sudoedit /etc/locale.gen
```

```
sudo locale-gen
```

## usage

<kbd>ctrl</kbd> + <kbd>i</kbd> to import stl files

#### how to set temperature

filament settings -> filament

These are different config settings that you have to save separately from the printer settings.
