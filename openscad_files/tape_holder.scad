HOOK_ANGLE = 45;
WALL_DISTANCE = 60;
BAR_RADIUS = 10;
HOLDER_LENGTH=120;

module tape_holder() {
	hull() {
		cylinder(h=HOLDER_LENGTH,r=BAR_RADIUS);

		translate([0,0,HOLDER_LENGTH]) {
			sphere(r=BAR_RADIUS*0.8);
		}
	}

	// hook
	translate([0,0,HOLDER_LENGTH]) {
		rotate([HOOK_ANGLE,0,0]) {
			cylinder(h=50, r=BAR_RADIUS*0.8);
		}
	}

	rotate([0,90,0]) {
		linear_extrude(WALL_DISTANCE) {
			circle(BAR_RADIUS);
		}
	}

	sphere(BAR_RADIUS);
}

module wall_mount(thickness) {
	difference() {
		union() {
			translate([WALL_DISTANCE-thickness, -BAR_RADIUS, 0]) {
				rotate([0,90,0]) {
					linear_extrude(thickness) {
						square([BAR_RADIUS*3,BAR_RADIUS*2]);
					}
				}
			}
			translate([WALL_DISTANCE-thickness, 0, -BAR_RADIUS*3]) {
				rotate([0,90,0]) {
					linear_extrude(thickness) {
						circle(BAR_RADIUS);
					}
				}
			}
		}

		// screw hole
		translate([WALL_DISTANCE-thickness, 0, -BAR_RADIUS*3]) {
			rotate([0,90,0]) {
				cylinder(h=thickness,d1=12,d2=3.5);
			}
		}
	}
}

tape_holder();

rotate([270,0,0]) {
	wall_mount(5);
}
rotate([90,0,0]) {
	wall_mount(5);
}
