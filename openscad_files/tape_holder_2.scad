HOOK_ANGLE = 45;
WALL_DISTANCE = 80;
BAR_RADIUS = 10;
HOLDER_LENGTH=120;

module tape_holder() {
	// first bar from wall
	translate([WALL_DISTANCE*0.8,BAR_RADIUS*1.2,0]) {
		cylinder(h=HOLDER_LENGTH,r=BAR_RADIUS);
	}

	// second bar from wall
	translate([WALL_DISTANCE*0.5,BAR_RADIUS*2,0]) {
		cylinder(h=HOLDER_LENGTH,r=BAR_RADIUS);
	}

	// third bar from wall
	translate([WALL_DISTANCE*0.2,BAR_RADIUS*1.2,0]) {
		cylinder(h=HOLDER_LENGTH,r=BAR_RADIUS);
	}

	// final bar
	hull() {
		cylinder(h=HOLDER_LENGTH,r=BAR_RADIUS);

		translate([0,0,HOLDER_LENGTH]) {
			sphere(r=BAR_RADIUS);
		}
	}

	// connect bars
	hull() {
		rotate([0,90,0]) {
			cylinder(h=WALL_DISTANCE, r=BAR_RADIUS);
			translate([0,BAR_RADIUS*2,0]) {
				cylinder(h=WALL_DISTANCE,r=BAR_RADIUS);
			}
		}
		translate([0,0,0]) {
			sphere(r=BAR_RADIUS);
		}
		translate([0,BAR_RADIUS*2,0]) {
			sphere(r=BAR_RADIUS);
		}
	}

	// left side bars connection
	hull() {
		translate([0,0,HOLDER_LENGTH]) {
			rotate([0,90,0]) {
				cylinder(h=WALL_DISTANCE, r=BAR_RADIUS);
			}
		}
		translate([0,BAR_RADIUS*2,HOLDER_LENGTH]) {
			rotate([0,90,0]) {
				cylinder(h=WALL_DISTANCE, r=BAR_RADIUS);
			}
		}
		translate([0,0,HOLDER_LENGTH]) {
			sphere(r=BAR_RADIUS);
		}
		translate([0,BAR_RADIUS*2,HOLDER_LENGTH]) {
			sphere(r=BAR_RADIUS);
		}
	}

	sphere(BAR_RADIUS);
}

module wall_mount(thickness) {
	difference() {
		union() {
			translate([WALL_DISTANCE-thickness, -BAR_RADIUS, 0]) {
				rotate([0,90,0]) {
					linear_extrude(thickness) {
						square([BAR_RADIUS*3,BAR_RADIUS*2]);
					}
				}
			}
			translate([WALL_DISTANCE-thickness, 0, -BAR_RADIUS*3]) {
				rotate([0,90,0]) {
					linear_extrude(thickness) {
						circle(BAR_RADIUS);
					}
				}
			}
		}

		// screw hole
		translate([WALL_DISTANCE-thickness, 0, -BAR_RADIUS*3]) {
			rotate([0,90,0]) {
				cylinder(h=thickness,d1=10,d2=4);
			}
		}
	}
}

tape_holder();

rotate([270,0,0]) {
	wall_mount(5);
}

translate([0,0,HOLDER_LENGTH]) {
	rotate([270,0,0]) {
		wall_mount(5);
	}
}
