#### wood glue mixed with water

Put some wood glue and water in a glass jar.\
Shake it until it is a white liquid.\
Heat up the bed of the printer to 60 degrees celsius.\
Apply the liquid with a big brush.\
Wait about 2-3 minutes until the wood glue is dried/cured.\
Apply another layer.\
Wait until it is dry.

Wipe off that dropped on floor or on the sides of the build plate.\
And you are ready to print.\
The brush can be easily cleaned with water.

Skirts are hard to remove when the build plate is prepared like this.\
Maybe use no slicer adhesion setting at all?

#### sleeping some time after heating bed

Waiting for 10 minutes after your heatbed is heated up improved the bed adhesion.\
It makes the heat spread more evenly.\
Although it was still not perfect.

Put this after your headbed heatup code:
```
G4 S600
```
After that you can start to heat up the nozzle and then do auto bed leveling.

5 minutes seem to be enough though, waiting 10 minutes is not necessary.

2 minutes were enough as well.

```
M117 wait for temp spread
G4 S120 ; wait x seconds to heat up bed equally
```

#### TODO: use brims and try different tools to remove it

Using brims was a good method in the past to make the print stick better to the build plate.\
But brims are annoying to remove and create an elephant foot.

Maybe sanding blocks or sanding sponges help?

Or maybe a wood plane?

deburring tools are supposed to work really good!\
=> will work in most situations, but when the first layer does not have \
straight outlines it might be difficult.\
But in that case all removing strategies will be labor intensive.

Exacto Knife: works ok, but not perfect\
Deburring tool: kind of works but you need a lot of pressure because \
the blades are not sharp enough

## FAILS

#### increasing initial layer height

Setting the "initial layer height" from 0.2 to 0.3 in `cura` actually \
destroyed the print.\
The second skirt line did not stick to the build plate at all and everything came off.\
Even though the cura setting says: "A thicker initial layer makes adhesion \
to the build plate easier."
