$fn=40;

module rounded_rect(length,width,filletradius) {
  hull() {
    translate([filletradius,filletradius,0]) {
      circle(r=filletradius);
    }
    
    translate([length-filletradius,width-filletradius,0]) {
      circle(r=filletradius);
    }
    
    translate([length-filletradius,filletradius,0]) {
      circle(r=filletradius);
    }
    
    translate([filletradius,width-filletradius,0]) {
      circle(r=filletradius);
    }
  }
}

rounded_rect(20,10,1);