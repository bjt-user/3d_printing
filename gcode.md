There are different variants of gcode.\
The common gcode flavour for 3d printing seems to be `marlin`.

https://marlinfw.org/meta/gcode/

#### M117 - print to lcd screen

```
M117 hello world!
```

#### G4 - dwell (sleep)
```
G4 S5 ; sleep for 5 seconds
```
or
```
G4 P500 ; sleep for 500ms
```

#### G28 - auto home

https://marlinfw.org/docs/gcode/G028.html

#### G0/G1 - linear move

By convention, most G-code generators use G0 for non-extrusion movements (those without the E axis) and G1 for moves that include extrusion. This is meant to allow a kinematic system to, optionally, do a more rapid uninterpolated movement requiring much less calculation.

You can also set a `feedrate` with this as shown in the examples.

https://marlinfw.org/docs/gcode/G000-G001.html

#### M400 - wait for moves to finish

This is especially useful when trying to find out what a certain command is doing.\
Or if you want to print debug messages before or after a command.

#### G90 - absolute positioning

#### G91 - relative positioning

The default seems to be absolute positioning.\
When you switch to relative positioning and do this:
```
G0 Z10
```
then you will not go to position Z10, the extruder will move up by 10.\
Because the current position is then seen as Z0.

#### M105 - Report Temperatures

This prints the current print time to the lcd on an ender 3 pro.

Not sure what this one does exactly.

#### M83 - E relative

Put E(xtrusion) Dimension to relative values independed of the other axes.

https://marlinfw.org/docs/gcode/M083.html
