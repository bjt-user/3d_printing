;FLAVOR:Marlin

G0 Z20 ;dont have hotend at bed while heating up

M117 heat up bed + hotend
; trying to heat up bed and hotend simultaneously
M140 S65  ; set bed temperature to 65°C and continue
M104 S200 ; set hot end temperature to 200°C and continue
M109 S200 ; wait for hot end temperature to reach 200°C
M190 S65  ; wait for bed temperature to reach 65°C

G92 E0 ; Reset Extruder
G28 ; Home all axes

G0 Y4

M83 ; relative extrusion mode

G1 F200
G1 X200 E30
G1 Y6
G1 X1 E30
M400

G92 E0 ; Reset Extruder
G0 F1000

G0 Z50
M400 ; wait until move finishes
M117 Done!
