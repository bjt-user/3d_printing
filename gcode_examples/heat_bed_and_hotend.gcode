;FLAVOR:Marlin

G0 Z20 ;dont have hotend at bed while heating up

; heat up bed and hotend simultaneously
M140 S65  ; set bed temperature to 65°C and continue
M104 S200 ; set hot end temperature to 200°C and continue
M109 S200 ; wait for hot end temperature to reach 200°C
M190 S65  ; wait for bed temperature to reach 65°C

G92 E0 ; Reset Extruder
G28 ; Home all axes

G0 Z50
M400 ; wait until move finishes
M117 Done!
