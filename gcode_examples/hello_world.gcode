;FLAVOR:Marlin
G4 S5 ; sleep for 5 seconds
M117 Lets wait for one second!
G4 P1000 ; sleep for 1000ms
M117 Lets wait for ten seconds!
G4 S10
M117 Done!
