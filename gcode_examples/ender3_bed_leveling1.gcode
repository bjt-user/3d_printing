;FLAVOR:Marlin
G92 E0 ; Reset Extruder

G0 Z20 ;dont have hotend at bed while heating up

M117 heat up bed + hotend
; trying to heat up bed and hotend simultaneously
M140 S65  ; set bed temperature to 65°C and continue
M104 S200 ; set hot end temperature to 200°C and continue
M109 S200 ; wait for hot end temperature to reach 200°C
M190 S65  ; wait for bed temperature to reach 65°C

G28 ; Home all axes

G0 Y5

M83 ; relative extrusion mode

G1 F400
; first line
G1 X233 E46.6 ; move from X0 to X233
G1 Y80 E15 ; move from Y5 to Y80
; second line
G1 X2 E46.2 ; move from X233 to X2
G1 Y155 E15 ; move from Y80 to Y155
; third line
G1 X233 E46.2 ; move from X2 to X233
G1 Y230 E15 ; move from Y155 to Y230
; fourth line
G1 X2 E46.2 ; move from X233 to X2
M400

G92 E0 ; Reset Extruder
G0 F1000

G0 Z50
M400 ; wait until move finishes
M117 Done!
