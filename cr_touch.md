#### ender 3 pro installation

1. level bed manually one last time coarsely in cold state \
(have a visible air gap between nozzle and bed, you will not get any closer \
on this aweful printer)
1. put binary of bl touch marlin firmware on sdcard via file manager (copy/paste)
PITFALL: DO **NOT FLASH** THE SDCARD WITH THE FIRMWARE IT WILL NOT WORK \
AND THEN YOU WILL HAVE TO REFORMAT THE SD CARD \
So go in your file manager and use ctrl c ctrl v to paste the binary to the usb
1. put sd card in printer, start printer, wait 10 seconds.\
you should now see "bilinear leveling" at "about printer"
1. manual installation of cr touch on the printer and motherboard... \
(tedious process, not described here)

#### z offset

After "Auto Home" the nozzle goes up 10mm.\
So theres no way to set the Z offset.

Also before installing a CR touch you should find a way to make the beeper quiet.
Electric tape helps for the beeper!

Setting the Z Offset value in the "bed leveling" menu does not have any effect.\
I found out that there is a "set home offsets" point in a different menu.\
But when hitting that an error appears: "err: too far".

PITFALL: The Z offset is **NOT** used by **"auto home"**, but it IS used when printing.\
And it is used to set where Z 0 is.\
So when you move to Z 0 that is the position of the CR Touch + the offset.

## troubleshotting

#### leveling cold vs. preheated

PITFALL: Leveling cold vs. leveling with heated nozzle and heatbed is a huge difference.\
But in both cases you run into problems, because with a prehreated nozzle you have \
some extrusion coming out of the nozzle which makes it almost impossible to \
go below the nozzle with a piece of paper or some other object.\
In cold state it is normal to have a big air gap between nozzle and surface.
